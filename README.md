# simPEL-EEG

The simPEL-EEG study aims to understand the neural correlates of prediction error and its relationship between recognition memory. The study consists of three sessions, while the first two sessions are done via Pavlovia.org, the last session is conducted at the Dipf EEG Lab. 

For the related literature, presentations, data analysis, protokols and informed consents please see file repository.
For the study manuals, please check [the wiki page](https://gitlab.com/Gozem_Turan/simpel-eeg/-/wikis/Manual_EEG).

**Study email: lisco_simpel-eeg@web.de
password: letseatmupir**
